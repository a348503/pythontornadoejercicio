
# Aprendiendo Micro web frameworks
En este repositorio se encuentra implementacion en código en Python,que es un servicio web simple construido utilizando el framework Tornado para manejar operaciones relacionadas con usuarios. El servicio incluye funcionalidad básica de CRUD (Crear, Leer, Actualizar, Eliminar) para administrar usuarios.

### Prerequisitos

Antes de ejecutar este código, asegúrarnos de tener las dependencias necesarias instaladas, incluyendo Tornado y asyncio.

Para instalar Tornado, puede utilizar pip:
    pip install tornado


### Instalación

1.Asegurarse de tener instaladas las dependencias correspondientes
2. Clonar este repositorio en tu computadora
    git clone https://gitlab.com/a348503/pythontornadoejercicio.git
3. Para ejecutar el servicio web, ejecute el script:
    python main.py
4. Una vez que el servicio está en funcionamiento, puede utilizar un cliente HTTP por ejemplo,un navegador web para interactuar con el servicio realizando solicitudes a los puntos finales especificados. 
Aquí se describen las operaciones admitidas:


    #### Crear un Usuario (POST):

    URL: /users
    Método: POST
    Crea un nuevo usuario y devuelve "Usuario creado".

    ####Leer Información de Usuario (GET):
    URL para un usuario específico: /users/{id}
    URL para listar todos los usuarios: /users
    Método: GET
    Recupera la información del usuario por ID o lista todos los usuarios según corresponda.
    
    ####Actualizar Información de Usuario (PUT):
    URL: /users/{id}
    Método: PUT
    Sobrescribe los datos de un usuario existente y devuelve "Usuario reemplazado con id: {id}".
    
    ####Modificar Información de Usuario (PATCH):
    URL: /users/{id}
    Método: PATCH
    Modifica los datos de un usuario existente y devuelve "Usuario actualizado con id: {id}".
    
    ####Eliminar Usuario (DELETE):
    URL: /users/{id}
    Método: DELETE
    Elimina un usuario por su ID y devuelve "Usuario eliminado con id: {id}".

## Construido con 

python3
tornado
asyncio


## Autores

Anahí Peinado Villalobos 353262 
Gilberto Contreras Conn 348503
Zaid Joel Gonzalez Mendoza 353254

## Licencia

No cuenta con licencia 
