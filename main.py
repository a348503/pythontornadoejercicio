import asyncio
import tornado #.web

class MainHandler(tornado.web.RequestHandler):
    def post(self):
        self.write("Usuario creado")
    def get(self, usr_id = None):
        if usr_id is None:
            self.write("Lista de usuarios")
        else:
            self.get2(usr_id)
    def get2(self,usr):
        self.write("Usuario con id: " + usr)
    def put(self, usr_id):
        self.write("Usuario reemplazado con id: " + usr_id)
    def patch(self, usr_id):
        self.write("Usuario actualizado con id: " + usr_id)
    def delete(self, usr_id):
        self.write("Usuario eliminado con id: " + usr_id)
    

'''         

POST  → Crear un objeto. → users/

GET → lee un objeto. → users/{id}
 
GET → lee una lista de objetos. → users/      

PUT → sobre escribe un objeto. → users/{id}

PATCH → modifica un objeto → users/{id}

DELETE → elimina un objeto → users/{id}

'''
    

def make_app():
    return tornado.web.Application([
        (r"/users", MainHandler),
        (r"/users/(\d+)",MainHandler)
    ])

async def main():
    app = make_app()
    app.listen(4000)
    await asyncio.Event().wait()

if __name__ == "__main__":
    asyncio.run(main())